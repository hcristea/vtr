import React, { Component } from 'react';
import './StockList.css';
import axios from 'axios';

class StockList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			dataset_code: this.props.defaultQuandlCode, 
			stocks: [
				{id: 'FB', text: 'Facebook Inc. (FB) Prices, Dividends, Splits and Trading Volume'}, 
				{id: 'GOOGL', text: 'Alphabet Inc (GOOGL) Prices, Dividends, Splits and Trading Volume'}
			]
		};

		this.handleChange = this.handleChange.bind(this);
		this.selectCode = this.selectCode.bind(this);
	};
	
	componentDidMount() {
		// Get quandl codes and build the select options
		axios.get('wiki-datasets-codes.json')
			.then(res => {
				// Get sorted stocks info sorted by company name
				let sorted = res.data.sort(function(a, b){
					let at = a.text.toUpperCase(), 
						bt = b.text.toUpperCase();
					return at < bt ? -1 : (at > bt ? 1 : 0);
				});
				this.setState({stocks: sorted});
			});
	};
	
	// Handler for select change event
	handleChange(event) {
		let selectedCode = event.target.value;
		this.setState({dataset_code: selectedCode});
		this.props.onStockChange(selectedCode, this.state.dataset_code, this.getStockTitle(selectedCode));
	};
	
	selectCode(code) {
		this.setState({dataset_code: code});
	};
	
	// Find the title for the stock with selected code
	getStockTitle(code) {
		let stocks = this.state.stocks, 
			i;
		for(i = 0; i < stocks.length; i++){
			if (stocks[i].id === code){
				return stocks[i].text;
			}
		}
		return '';
	};
	
	render() {
		return (
			<div className="StockList">
				<form id="frm">
					<label htmlFor="dataset_code">Stocks</label>
					<select value={this.state.dataset_code} ref='dataset_code' name="dataset_code" onChange={this.handleChange} className="form-control" data-data-url="wiki-datasets-codes.json">
						{this.state.stocks.map(stock =>
							<option key={stock.id} value={stock.id}>{stock.text}</option>
						)}
					</select>
				</form>
			</div>
		);
	};
};

export default StockList;
