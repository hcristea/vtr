import React, { Component } from 'react';
import './History.css';

class History extends Component {
	constructor(props) {
		super(props);
		this.state = {
			codes: []
		};

		this.add = this.add.bind(this);
		this.handleClick = this.handleClick.bind(this);
	};
	
	// add the code to history
	add(code) {
		let codes = this.state.codes;
		codes.unshift(code);
		this.setState({codes: codes});
	};
	
	handleClick(code, event) {
		event.preventDefault();
		this.props.onHistoryClick(code);
	};
	
	render() {
		return (
			<div className="History">
				<strong>Previously viewed codes:</strong>
				{this.state.codes.map((code, ix) =>
					<button className="btn btn-primary btn-xs" key={ix} onClick={this.handleClick.bind(this, code)}>{code}</button>
				)}				
			</div>
		);
	};
};

export default History;
