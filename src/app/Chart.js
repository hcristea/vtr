import React, { Component } from 'react';
import './Chart.css';
import Dygraph from 'dygraphs';

class Chart extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data: null
		};
		
		this.g = null;
	};

	componentWillUnmount() {
		if (this.g){
			this.g.destroy();
		}
		this.g = null;
	};
	
	displayChart(data, code, title) {
		let preparedData = this.prepareData(data, code);
		this.g = new Dygraph(document.getElementById("chart"), preparedData, {
			title : title,
			ylabel : 'Price (USD)',
			legend : 'always',
			color: ['#008000'],
			showRangeSelector : true, 
			rangeSelectorHeight : 60,
			gridLineColor: '#d4d4d4'
		});
	};
	
	// return a proper CVS string from data
	prepareData(data, code) {
		let ret = "Date," + code, 
			sorted = data.sort(function(a, b){
				return a[0] < b[0] ? -1 : (a[0] > b[0] ? 1 : 0);
			});
		for(var i = 0; i < sorted.length; i++){
			ret += "\n" + sorted[i][0].replace(/-/g, '') + ',' + sorted[i][1];
		}
		return ret;
	};
		
	render() {
		return (
			<div className="Chart well">
				<div id="chart" className="Chart-graphWrapper"></div>
			</div>
		);
	};
};

export default Chart;
