import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import StockList from './app/StockList';
import History from './app/History';
import Chart from './app/Chart';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			defaultQuandlCode: 'GOOGL', 
			defaultStockTitle: 'Alphabet Inc (GOOGL) Prices, Dividends, Splits and Trading Volume'
		};
		
		this.quandlData = {
			// Quandl API Key, 
			api_key: 'MJLLpq9GgX47-vMjnjvB',
			// Close price
			column_index: 4
		};
		
		// Template URL to get stock data
		// {datasetCode} will be replace by the selected Quandl code.		
		this.tplUrl = 'https://www.quandl.com/api/v3/datasets/WIKI/{datasetCode}/data.json';
		
		// Set the scope to App instance
		this.handleStockChange = this.handleStockChange.bind(this);
		this.handleHistoryClick = this.handleHistoryClick.bind(this);
	};
	
	// Fetch price data for default Quandl code and display it
	componentDidMount() {
		this.getStockPrice(this.state.defaultQuandlCode, this.state.defaultStockTitle);
	};
	
	// Handler for stock list change
	handleStockChange(codeNew, codeOld, titleNew) {
		this.history.add(codeOld);
		this.getStockPrice(codeNew, titleNew);
	};
	
	// Handler for History click
	handleHistoryClick(code) {
		this.stockList.selectCode(code);
		this.getStockPrice(code, this.stockList.getStockTitle(code));
	};
	
	// Returns the formatted Quandl endpoint uri from whre to get the data.
	getQuandlUrl(code) {
		let url = this.tplUrl.replace('{datasetCode}', code), 
			qd = this.quandlData;
		return url + '?api_key=' + qd.api_key + '&column_index=' + qd.column_index;
	};
	
	// Make an XHR request and get the data.
	getStockPrice(code, title) {
		axios.get(this.getQuandlUrl(code))
			.then(response => {
				this.chart.displayChart(response.data.dataset_data.data, code, title);
			})
			.catch(error => {
				// just output the error in console.
				console.log(error);
			});
	};
	
	render() {
		return (
			<div className="container App">
				<header className="App-header">
					<h1 className="App-title">Bob's Stock Price Viewer</h1>
				</header>
				<div className="well well-sm">
					<StockList defaultQuandlCode={this.state.defaultQuandlCode} ref={instance => {this.stockList = instance;}} onStockChange={this.handleStockChange}></StockList>
					<History ref={instance => {this.history = instance;}} onHistoryClick={this.handleHistoryClick}></History>
				</div>
				<Chart ref={instance => {this.chart = instance;}}></Chart>
			</div>
		);
	};
};

export default App;
